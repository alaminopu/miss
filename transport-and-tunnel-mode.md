# Difference between the Tunnel and Transport modes

## Answer:

The difference between the Tunnel and Transport modes are given below

| Tunnel Mode | Transport mode |
| ------ | ------ |
| Tunnel mode protects the internal routing information by encrypting the IP header of the original packet. The original packet is encapsulated by a another set of IP headers. | The transport mode encrypts only the payload and ESP trailer; so the IP header of the original packet is not encrypted. |
| It is widely implemented in site-to-site VPN scenarios. | The IPsec Transport mode is implemented for client-to-site VPN scenarios. |
| NAT traversal is supported with the tunnel mode. | NAT traversal is not supported with the transport mode. |
| Additional headers are added to the packet; so the payload MSS is less. | MSS is higher, when compared to Tunnel mode, as no additional headers are required. |



